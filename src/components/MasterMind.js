import React, {useState, useEffect} from 'react';
import utils from "../utils/utils";

const Rules = (props) => {
    return <div style={{display: props.display ? '' : 'none'}}>
        Intenta adivinar el patrón, tanto en color como en posición, en diez turnos. Después de
        enviar una linea, en el cuadro aparecerá en negro si la respuesta es correcta tanto en color
        como en posición, uno blanco si sólo en color pero no en posición y una {'\u2A2F'} si nada coincide.
    </div>
}

const SubmitResults = ({display, onClick, row}) => {
    return (
        <button className="checkBttn" style={{display: display?"":"none" }} onClick={()=>{onClick(row)}}> {'\u261B'} </button>
    );
}

const PlayCards = ({oportunities, onClick, currentColor, paintColor, results, displaySubmits, evaluateSubmitOnClick}) => {
    return (
        <>
            {utils.range(0,oportunities-1).map(el =>
        <div key={el} className="rectangle">
            <div className="circle1" onClick={ ()=> onClick(el,0,paintColor)} style={{backgroundColor:currentColor[el][0]}}/>
            <div className="circle2" onClick={ ()=> onClick(el,1,paintColor)} style={{backgroundColor:currentColor[el][1]}}/>
            <div className="circle3" onClick={ ()=> onClick(el,2,paintColor)} style={{backgroundColor:currentColor[el][2]}}/>
            <div className="circle4" onClick={ ()=> onClick(el,3,paintColor)} style={{backgroundColor:currentColor[el][3]}}/>
            <SubmitResults display={displaySubmits[el]} onClick={evaluateSubmitOnClick} row={el} />
            <ResultSquare results={results[el]}/>
        </div>
    )}
    </>
);
}

const ResultSquare = ( {results}) => {
    return (
        <div className="square">
            <div className="minicircle1" style={{backgroundColor:results[0]==='correct'?"black":""}}> {results[0] === 'incorrect'?'\u2A2F':''} </div>
            <div className="minicircle2" style={{backgroundColor:results[1]==='correct'?"black":""}}> {results[1] === 'incorrect'?'\u2A2F':''} </div>
            <div className="minicircle3" style={{backgroundColor:results[2]==='correct'?"black":""}}> {results[2] === 'incorrect'?'\u2A2F':''}</div>
            <div className="minicircle4" style={{backgroundColor:results[3]==='correct'?"black":""}}> {results[3] === 'incorrect'?'\u2A2F':''}</div>
        </div>
    );
}


const MasterMind = () => {
    const opportunities = 10;
    const colorPalette = ["#ffbf00",
        "#f6e665",
        "#8cf285",
        "#32bcff",
        "#ab70ea",
        "#fe019a"];
    const [gameWin, ] = useState(() => {
        let myColorPalette = [...colorPalette];
        const gamePlayPalette = [];
        for (let i = 0; i < 4; i ++) {
            const index = utils.random(0, myColorPalette.length-1);
            gamePlayPalette.push(myColorPalette[index]);
            myColorPalette = [...myColorPalette.slice(0, index), ... myColorPalette.slice(index+1)];
        }
        return gamePlayPalette;
        });

    const [displayRules, setDisplayRules] = useState(false);

    const [displaySubmits, setDisplaySubmits] = useState( () => {
            const myArray = [];
            utils.range(0, opportunities - 1).forEach(el => myArray.push(false));
            return myArray;
        }
    );
    const [currentColor, setCurrentColor ] = useState(() => {
        const myArray =[];
        utils.range(0, opportunities-1).forEach(el => {myArray.push(["#fff","#fff","#fff","#fff"])});
        return myArray;});
    const [results, setResults] = useState(() => {
        const myArray = [];
        utils.range(0, opportunities-1).forEach(el => {myArray.push(["unplay","unplay","unplay","unplay"])});
        return myArray;
    });


    const evaluateSubmit = (row) => {
        for (let i = 0; i < gameWin.length; i++) {
            //The color is present in the result
            if(currentColor[row].includes(gameWin[i])) {
                results[row][i] = 'present';
                if(currentColor[row][i]=== gameWin[i]) {
                    results[row][i] = 'correct';
                }
                continue;
            }
            results[row][i] = 'incorrect';
        }
        setResults([...results])
    };


    const [paintColor, setPaintColor] = useState("#fff");

    const fillColorOnClick = (row, column, paintColor) => {
        currentColor[row][column] = paintColor;
        setCurrentColor([...currentColor]);
        if (currentColor[row].filter(el => el !== "#fff").length === 4){
            displaySubmits[row] = true;
            setDisplaySubmits([... displaySubmits]);
        }
    }

    return (
        <div>
            <div className="completeHeader">
      <span className="colorHeader">
        <span className="letter" style={{backgroundColor: colorPalette[0]}}>M</span>
        <span className="letter" style={{backgroundColor: colorPalette[1]}}>A</span>
        <span className="letter" style={{backgroundColor: colorPalette[2]}}>S</span>
        <span className="letter" style={{backgroundColor: colorPalette[3]}}>T</span>
        <span className="letter" style={{backgroundColor: colorPalette[4]}}>E</span>
        <span className="letter" style={{backgroundColor: colorPalette[5]}}>R</span>
      </span>
                <span className="monochromeHeader">MIND</span>
            </div>
            <div className="rules">
                <h3 onClick={() => {
                    setDisplayRules(!displayRules)
                }}>
                    {!displayRules ? 'Mostrar Reglas' : 'Ocultar Reglas'}
                </h3>
                <Rules display={displayRules}/>
            </div>
            <div className="board">
                <div id="middlebox">
                   <PlayCards oportunities={opportunities}
                              onClick={fillColorOnClick}
                              currentColor={currentColor}
                              paintColor={paintColor}
                              results={results}
                              displaySubmits={displaySubmits}
                              evaluateSubmitOnClick={evaluateSubmit}
                   />
                </div>
                <div id="rightbox">
                    <div className="controlRectangle">
                        <div className="circle1" onClick={() => {setPaintColor(colorPalette[0])}} />
                        <div className="circle2" onClick={() => {setPaintColor(colorPalette[1])}} />
                        <div className="circle3" onClick={() => {setPaintColor(colorPalette[2])}} />
                        <div className="circle4" onClick={() => {setPaintColor(colorPalette[3])}} />
                        <div className="circle5" onClick={() => {setPaintColor(colorPalette[4])}} />
                        <div className="circle6" onClick={() => {setPaintColor(colorPalette[5])}} />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default MasterMind;